# Harjoitus1

Luo tavallinen HTML -sivun perurakenne ja lisää teksti `<body>` -osaan. Saat käytä tehtävässä seuraavat elementit: `<h3>`, `<p>`, `<strong>`, `<em>`, `<a>` ja `<img>`.

Malli:
![Malli](Harjoitus1.jpg)

Tekstin lähde on: [Mutinan sota - Wikipedia](https://fi.wikipedia.org/wiki/Mutinan_sota)

Kuvan lähde on: https://upload.wikimedia.org/wikipedia/commons/6/6b/Illus266_-_Marcus_Antonius.png



